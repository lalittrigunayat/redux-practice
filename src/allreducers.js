import counterReducer from './Reducers/counterReducer.js'
import authReducer from './Reducers/AuthReducer.js'
import {combineReducers} from 'redux'

const allReducers = combineReducers({
    counter:counterReducer,
    authenticate:authReducer
})

export default allReducers;