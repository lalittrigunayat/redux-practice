import React from 'react'
import {useSelector,useDispatch} from 'react-redux'
import { increment ,decrement ,authentication} from '../Actions/action.js'

function Home(){
    const counter = useSelector(state => state.counter);
    const auth = useSelector(state => state. authenticate);
    const dispatch = useDispatch();
    return(

        <div className="Home">
            <h1>Counter {counter}</h1>
            <button onClick={() => dispatch(increment())}>+</button>
            <button onClick={() => dispatch(decrement())}>-</button>
            {auth?<h2>use are logged in: <button onClick={() => dispatch(authentication())}>logout</button></h2>:<h2>you are not looged in : <button onClick={() => dispatch(authentication())}>login</button></h2>}
        </div>
    )
}
export default Home;